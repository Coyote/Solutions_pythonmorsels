def parse_ranges(sequences):
    pairs = (
        partitioning('-', group)
        for group in sequences.split(',')
    )
    print(pairs)
    return (
        value
        for start, stop in pairs
        for value in range(int(start), int(stop)+1)
    )

def partitioning(sep, group):
    a, _, b = group.partition(sep)
    return ((a,b) if b.isnumeric() else (a,a))

def test_iterator(iterator):
    while True:
        try:
            print(next(iterator))
        except StopIteration:
            break


if __name__ == "__main__":
    test1 = parse_ranges('1-2,4-4,8-13')
    test_iterator(test1)
    # [1, 2, 4, 8, 9, 10, 11, 12, 13]
    test1 = parse_ranges('0-0, 4-8, 20-20, 43-45')
    test_iterator(test1)
    # [0, 4, 5, 6, 7, 8, 20, 43, 44, 45]

    test1 = parse_ranges('0,4-8,20,43-45')
    test_iterator(test1)
    # [0, 4, 5, 6, 7, 8, 20, 43, 44, 45]
