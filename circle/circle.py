import math

class Circle:

    def __init__(self, radius=1):
        self._radius = radius
        self._diameter = radius*2

    @property
    def radius(self):
        return self._radius
    @radius.setter
    def radius(self, radius):
        self._radius = radius
        self._diameter = radius*2

    @property
    def diameter(self):
        return self._diameter
    @diameter.setter
    def diameter(self, diameter):
        self._diameter = diameter
        self._radius = diameter*(1/2)

    @property
    def area(self):
        return self._radius**2*math.pi
        
    def __repr__(self):
        return f"Circle({self._radius})"


if __name__=="__main__":
    c = Circle()
    print(c.radius)
    print(c.diameter)
    print(c.area)
    c.radius=2
    print(c.radius)
    print(c.diameter)
    print(c.area)
    c.diameter=2
    print(c.radius)
    print(c.diameter)
    print(c.area)
    c.area=10

