import re

def count_words(phrase):
    phrase = phrase.split(" ")
    count_words = {}
    for word in phrase:
        word = clean_word_only_letters(word)
        word = word.lower()
        if word in count_words.keys():
            count_words[word] += 1
        else:
            count_words[word] = 1
    return count_words

def clean_word_only_letters(word):
    regex = r'[a-zA-Z\']'
    clean_word = "".join(re.findall(regex, word))
    return clean_word


if __name__ == "__main__":
    test1 = count_words("oh what a day what a lovely day")
    print(test1)
    #{'oh': 1, 'what': 2, 'a': 2, 'day': 2, 'lovely': 1}
    test1 = count_words("don't stop believing")
    print(test1)
    #{"don't": 1, 'stop': 1, 'believing': 1}
    test1 = count_words("The Queen will address the House of Commons today")
    print(test1)
    test1 = count_words("Oh what a day, what a lovely day!")
    print(test1)
    # {'oh': 1, 'what': 2, 'a': 2, 'day': 2, 'lovely': 1}
