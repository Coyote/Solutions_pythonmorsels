#!/usr/bin/python3
def tail(sequence, number):
    """
    Return the last n items of given iterable.
    """
    sub_sequence = []
    if number <= 0:
        return sub_sequence
    elif number == 1:
        index = slice(0, 0)
    else:
        index = slice(-(number-1), None)
    for item in sequence:
        sub_sequence = [*sub_sequence[index], item]
    sub_sequence = list(sub_sequence)
    return sub_sequence

if __name__ == "__main__":
    print(tail([1, 2, 3, 4, 5], 3))
    print(tail([1, 2, 3, 4, 5], -5))
    # [3, 4, 5]
    print(tail('hello', 2))
    # ['l', 'o']
    print(tail('hello', 0))
    # []
    nums = (n**2 for n in [1, 2, 3, 4])
    print(tail(nums, 2))
    # [9, 16]  # Consuming the generator
