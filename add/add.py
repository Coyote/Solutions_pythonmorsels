#!/usr/bin/python3
def add(*matrixes):
    final_matrix = []
    for ii, matrix in enumerate(matrixes):
        if ii == 0:
            final_matrix = matrix
        else:
            final_matrix = add_two(final_matrix, matrix)
    return final_matrix

def add_two(matrix1 : list, matrix2 : list) : 
    """Add corresponding numbers in given 2-D matrices."""
    test_size(matrix1, matrix2)
    matrix_add = []
    for row1,row2 in zip(matrix1, matrix2):
        test_size(row1,row2)
        matrix_add.append([
            value1 + value2
            for value1,value2 in zip(row1,row2)
        ])
    return matrix_add

def test_size(a, b):
    if len(a) != len(b):
        raise ValueError ("Given matrices are not the same size.")

if __name__ == "__main__":
    matrix1 = [[1, 9], [7, 3]]
    matrix2 = [[5, -4], [3, 3]]
    matrix3 = [[2, 3], [-3, 1]]
    matrix4 = add(matrix1, matrix2, matrix3)
    print(matrix4)
    # [[8, 8], [7, 7]]
