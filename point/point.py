class Point:

    """Three dimensional point."""

    def __init__(self, x, y, z):
        self.x, self.y, self.z  = x, y, z

    def __repr__(self):
        """ Return dev-readable representation of Point. """
        return f"Point(x={self.x}, y={self.y}, z={self.z})"

    def __eq__(self, other):
        """ Return True if our point is equal to the other point."""
        if isinstance(other, Point):
            return (self.x, self.y, self.z) == (other.x, other.y, other.z)
        return NotImplemented

    def __add__(self, other):
        """Return copy of our point, shifted by other."""
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y, self.z + other.z) 
        return NotImplemented

    def __sub__(self, other):
        """Return copy of our point, shifted by other."""
        if isinstance(other, Point):
            return Point(self.x - other.x, self.y - other.y, self.z - other.z) 
        return NotImplemented

    def __mul__(self, scalar):
        """Return new copy of our point, scaled by given value."""
        if not isinstance(scalar, (int, float)):
            return NotImplemented
        return Point(self.x * scalar, self.y * scalar, self.z * scalar) 

    __rmul__=__mul__

    def __iter__(self):
        return iter((self.x, self.y, self.z))

if __name__=="__main__":
    p1 = Point(1, 2, 3)
    print(p1)
    # Point(x=1, y=2, z=3)
    p2 = Point(1, 2, 3)
    print(p1 == p2)
    # True
    p2.x = 4
    print(p1 == p2)
    # False
    print(p2)
    # Point(x=4, y=2, z=3)

    p1 = Point(1, 2, 3)
    p2 = Point(4, 5, 6)
    print(p1 + p2)
    # Point(x=5, y=7, z=9)
    p3 = p2 - p1
    print(p3)
    # Point(x=3, y=3, z=3)

    p1 = Point(1, 2, 3)
    p2 = p1 * 2
    print(p2)
    # Point(x=2, y=4, z=6)

    p1 = Point(1, 2, 3)
    x, y, z = p1
    print(x, y, z)
    # (1, 2, 3)


    
