# from collections import Counter
from unicodedata import normalize


def clean_string(string):
    """Return a string withour white spaces, lowercased, only alphabetical and without accents."""
    string = string.replace(" ", "")
    string = string.lower()
    string = ''.join(filter(str.isalpha, string))
    new_string = ''
    for letter in string:
        letter = normalize("NFD", letter)[0]
        new_string = new_string + letter
    return new_string


def is_anagram(string1, string2):
    """Compare two strings to see if they are anagrams"""
    string1 = clean_string(string1)
    string2 = clean_string(string2)
    # IF the length of strings aint the same, they cannot be anagrams.
    if len(string1) == len(string2):
        if sorted(string1) == sorted(string2):
            return True
    return False


# def is_anagram2(string1, string2):
#     string1 = Counter(string1)
#     string2 = Counter(string2)
#     for key, value in string1.items():
#         if value != string2[key]:
#             return False
#     return True


if __name__ == "__main__":
    print(is_anagram("tea", "eat"))
    print(is_anagram("tea", "treat"))
    print(is_anagram("sinks", "skin"))
    print(is_anagram("Listen", "silent"))
    print(is_anagram("nine", "eine"))
    print(is_anagram("coins kept", "in pockets"))
    print(is_anagram("William Shakespeare", "I am a weakish speller"))
